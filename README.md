# Purpleish Theme

Purpleish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-purpleish_code.png](./images/sjsepan-purpleish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-purpleish_codium.png](./images/sjsepan-purpleish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-purpleish_codedev.png](./images/sjsepan-purpleish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-purpleish_ads.png](./images/sjsepan-purpleish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-purpleish_theia.png](./images/sjsepan-purpleish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-purpleish_positron.png](./images/sjsepan-purpleish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/11/2025
